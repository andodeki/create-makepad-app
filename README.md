cargo run my-app

<img src="https://makepad.nl/img/logo_makepad.svg" alt="Rapidly scaffold out a new Makepad app project." />

# Usage

## Cargo:

```bash
cargo install create-makepad-app
cargo create-makepad-app
```

<br>

```bash
# cargo
cargo create-makepad-app my-app --template counter --manager cargo
```

Currently supported template presets include:

- `counter`
- `simple stack navigation`
- `simple login`

You can use `.` for the project name to scaffold in the current directory.

## Semver

**create-makepad-app** is following [Semantic Versioning 2.0](https://semver.org/).

## Licenses

Code: (c) 2022.

MIT or MIT/Apache 2.0 where applicable.
