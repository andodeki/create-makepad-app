# Usage

## Cargo:

```bash
cargo install create-makepad-app
cargo create-makepad-app
```

# Clone this repository Need help cloning? Visit Help.
https://gitdab.com/andodeki/makepad-template.git
# Creating a new repository on the command line
touch README.md
git init
git checkout -b main
git add README.md
git commit -m "first commit"
git remote add origin https://gitdab.com/andodeki/makepad-template.git
git push -u origin main
# Pushing an existing repository from the command line
git remote add origin https://gitdab.com/andodeki/makepad-template.git
git push -u origin main

# Desktop in Debug Mode
cargo run

# Desktop in Release Mode
cargo run --release

# Desktop in small size
cargo run --profile=small

# Android
cargo makepad android run --release

# IOS Simulator
cargo makepad apple ios --org=my.test --app=makepad-template run-sim --release

# IOS Device
cargo makepad apple ios --org-id=123456 --org=my.test --app=makepad-template run-device makepad-template --release

# Cargo Check Builds
cargo makepad check install-toolchain
cargo makepad check all

cargo makepad wasm install-toolchain
cargo makepad apple ios install-toolchain
cargo makepad android --abi=all install-toolchain