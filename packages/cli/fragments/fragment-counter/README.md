# Makepad UI

This template should help get you started developing with Makepad Rust UI.

## Recommended IDE Setup

[VS Code](https://code.visualstudio.com/) + [rust-analyzer](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer).


# Desktop in Debug Mode
cargo run

# Desktop in Release Mode
cargo run --release

# Desktop in small size
cargo run --profile=small

# Android
cargo makepad android run --release

# IOS Simulator
cargo makepad apple ios --org=my.test --app=makepad-template run-sim --release

# IOS Device
cargo makepad apple ios --org-id=123456 --org=my.test --app=makepad-template run-device makepad-template --release

# Cargo Check Builds
cargo makepad check install-toolchain
cargo makepad check all

cargo makepad wasm install-toolchain
cargo makepad apple ios install-toolchain
cargo makepad android --abi=all install-toolchain